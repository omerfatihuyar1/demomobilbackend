import * as express from 'express';
import postresdb from "./models";

const app = express();

const PORT = process.env.PORT || 3000;

const userRouter = require('./router/userRouter.ts');
const productRouter = require('./router/productRouter.ts');
const authRouter = require('./router/authRouter.ts');
const rolesRouter = require('./router/rolesRouter.ts');
const productCategoryRouter = require('./router/productCategoryRouter.ts');
const orderRouter = require('./router/orderRouter.ts');

app.use(express.json());

app.use('/api/users', userRouter);
app.use('/api/products', productRouter);
app.use('/api/auth', authRouter);
app.use('/api/roles', rolesRouter);
app.use('/api/productCategory', productCategoryRouter);
app.use('/api/order', orderRouter);


/**
 * PosgreSQL Sequelize 
 */
postresdb.sequelize.sync({alter:true})
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ error: 'Internal Server Error' });
});

app.use((req, res, next:any) => {
  res.status(404).json({ error: 'Not Found' });
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
}); 