module.exports = (sequelize, DataTypes) => {
    const Orders = sequelize.define(
      "Orders",
      {
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          primaryKey: true,
          defaultValue: DataTypes.UUIDV4,
        },
        userId: {
          type: DataTypes.UUID,
          allowNull: false,
        },
        quantity: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        totalPrice: {
          type: DataTypes.DECIMAL(10, 2),
          allowNull: false,
        },
        orderDate: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        status: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        productList:{
          type: DataTypes.JSON,
          allowNull: false,
        }
      },
      {
        tableName: "orders",
        timestamps: false,
      }
    );
  
    
  
    return Orders;
  };
  