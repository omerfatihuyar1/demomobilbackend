module.exports = (sequelize, DataTypes) => {
    const Products = sequelize.define(
      "Products",
      {
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          primaryKey: true,
          defaultValue: DataTypes.UUIDV4,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        price: {
          type: DataTypes.DECIMAL(10, 2),
          allowNull: false,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        imageUrl: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        createDate: {
          type: DataTypes.DATE,
          allowNull: false,
        },
      },
      {
        tableName: "products",
        timestamps: false,
      }
    );
  
    return Products;
  };