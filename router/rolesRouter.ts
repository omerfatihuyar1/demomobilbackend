import * as express from 'express';
import db from '../models/index';

var Roles = db.roles;

const roleRouter = express.Router();

roleRouter.get('/', async (req, res) => {
  try {
    const roles = await Roles.findAll();
    res.json(roles);
  } catch (error) {
    console.error('Failed to fetch roles:', error);
    res.status(500).json({ error: 'An error occurred while fetching roles' });
  }
});

roleRouter.post('/', async (req, res) => {
  try {
    const { name, description, permissions } = req.body;
    const role = await Roles.create({ name, description, permissions });
    res.status(201).json(role);
  } catch (error) {
    console.error('Failed to create role:', error);
    res.status(500).json({ error: 'An error occurred while creating role' });
  }
});

roleRouter.get('/:roleId', async (req, res) => {
  try {
    const roleId = req.params.roleId;
    const role = await Roles.findByPk(roleId);
    if (!role) {
      res.status(404).json({ error: 'Role not found' });
    } else {
      res.json(role);
    }
  } catch (error) {
    console.error('Failed to fetch role:', error);
    res.status(500).json({ error: 'An error occurred while fetching role' });
  }
});

roleRouter.put('/:roleId', async (req, res) => {
  try {
    const roleId = req.params.roleId;
    const { name, description, permissions } = req.body;
    const role = await Roles.findByPk(roleId);
    if (!role) {
      res.status(404).json({ error: 'Role not found' });
    } else {
      role.name = name;
      role.description = description;
      role.permissions = permissions;
      await role.save();
      res.json(role);
    }
  } catch (error) {
    console.error('Failed to update role:', error);
    res.status(500).json({ error: 'An error occurred while updating role' });
  }
});

roleRouter.delete('/:roleId', async (req, res) => {
  try {
    const roleId = req.params.roleId;
    const role = await Roles.findByPk(roleId);
    if (!role) {
      res.status(404).json({ error: 'Role not found' });
    } else {
      await role.destroy();
      res.json({ message: 'Role deleted successfully' });
    }
  } catch (error) {
    console.error('Failed to delete role:', error);
    res.status(500).json({ error: 'An error occurred while deleting role' });
  }
});

module.exports = roleRouter;
