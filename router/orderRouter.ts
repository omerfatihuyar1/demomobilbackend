import * as express from "express";
import { v4 as uuidv4 } from "uuid";
import db from "../models/index";

var Orders = db.orders;

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const orders = await Orders.findAll();
    res.json(orders);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.get("/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const orders = await Orders.findAll({ where: { userId: id } });
    if (orders) {
      res.json(orders);
    } else {
      res.status(404).json({ error: "Order not found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.post("/", async (req, res) => {
  try {
    const { userId, quantity, totalPrice, orderDate, status, productList } =
      req.body;
    const order = await Orders.create({
      id: uuidv4(),
      userId,
      quantity,
      totalPrice,
      orderDate,
      status,
      productList,
    });
    res.json(order);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.put("/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const { userId, quantity, totalPrice, orderDate, status, productList } =
      req.body;
    const order = await Orders.findByPk(id);
    if (order) {
      order.userId = userId;
      order.quantity = quantity;
      order.totalPrice = totalPrice;
      order.orderDate = orderDate;
      order.status = status;
      order.productList = productList;
      await order.save();
      res.json(order);
    } else {
      res.status(404).json({ error: "Order not found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const order = await Orders.findByPk(id);
    if (order) {
      await order.destroy();
      res.json({ message: "Order deleted" });
    } else {
      res.status(404).json({ error: "Order not found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
