import * as express from "express";
import db from "../models/index";

const router = express.Router();

var Users = db.users;
var Roles = db.roles;

router.get("/", async (req, res) => {
  try {
    const users = await Users.findAll();
    res.json(users);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
});
router.get("/id/:id", async (req, res) => {
  try {
    const id = req.params.id;
    let user = await Users.findOne({
      where: { id },
      include: [{ model: Roles }],
    });
    user = user.get({ plain: true });
    if (user) {
      const { id, username, phone, createDate } = user;
      const roleName = user.Role.name;
      console.log(user);

      res.json({
        id,
        username,
        phone,
        createDate,
        roleName,
      });
    } else {
      res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});


router.get("/phone/:phone", async (req, res) => {
  try {
    const phone = req.params.phone;
    let user = await Users.findOne({
      where: { phone },
      include: [{ model: Roles }],
    });
    user = user.get({ plain: true });
    if (user) {
      const { id, username, phone, createDate } = user;
      const roleName = user.Role.name;
      console.log(user);

      res.json({
        id,
        username,
        phone,
        createDate,
        roleName,
      });
    } else {
      res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.put("/:id", async (req, res) => {
  try {
    const userId = req.params.id;
    const { name, email } = req.body;
    const user = await Users.findByPk(userId);
    if (user) {
      user.name = name;
      user.email = email;
      await user.save();
      res.json(user);
    } else {
      res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.delete("/users/:id", async (req, res) => {
  try {
    const userId = req.params.id;
    const user = await Users.findByPk(userId);
    if (user) {
      await user.destroy();
      res.json({ message: "User deleted successfully" });
    } else {
      res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
