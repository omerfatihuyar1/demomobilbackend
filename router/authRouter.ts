import * as express from 'express';
import db from '../models/index';

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");

var Users = db.users;
var Roles = db.roles;

const authRouter = express.Router();

authRouter.post("/signup", async (req, res) => {
  try {
    const { username, password, phone } = req.body;

    const existingUser = await Users.findOne({ where: { username } });
    if (existingUser) {
      return res.status(409).json({ error: "Username already exists" });
    }

    const hashedPassword = await bcrypt.hash(password, 10); // Şifre hashleme
    await Users.create({ username, phone, hash: hashedPassword });

    res.json({ message: "Users registered successfully" });
  } catch (error) {
    console.error("Signup error:", error);
    res.status(500).json({ error: "An error occurred during signup" });
  }
});

authRouter.post("/login", async (req, res) => {
    try {
      // Giriş bilgilerini al
      const { username, password } = req.body;
  
      // Kullanıcıyı bul
      let user = await Users.findOne({
        where: { username: username },
        include: [{ model: Roles }], 
      });
      user = user.get({plain:true});
      if (!user) {
        return res.status(401).json({ error: "Invalid username or password" });
      }
      const passwordMatch = await bcrypt.compare(password, user.hash);
      if (!passwordMatch) {
        return res.status(401).json({ error: "Invalid username or password" });
      }
      // Token oluşturma
      const secretKey = generateSecretKey();
      const token = jwt.sign({ userId: user.id,userRole:user.Role.name}, secretKey, { expiresIn: "1h" });
  
      // Token'i kullanıcı tablosunda güncelleme
      await Users.update({ token: token }, { where: { id: user.id } });
  
      res.json({ token });
    } catch (error) {
      console.error("Login error:", error);
      res.status(500).json({ error: "An error occurred during login" });
    }
  });
  

// Kullanıcı çıkışı
authRouter.post("/logout", async (req, res) => {
  try {
    const token = req.headers.authorization?.split(" ")[1]|| '';
    const user = await Users.findOne({ where: { token : token } });
    if (!user) {
      return res.status(403).json({ error: "Invalid token" });
    }

    user.token = null;
    await user.save();

    res.json({ message: "Users logged out successfully" });
  } catch (error) {
    console.error("Logout error:", error);
    res.status(500).json({ error: "An error occurred during logout" });
  }
});

const generateSecretKey = () => {
  return crypto.randomBytes(32).toString("hex");
};

module.exports = authRouter;
