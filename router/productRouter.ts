import * as express from 'express';
import db from '../models/index';

var Products = db.products;
var ProductsCategory = db.productCategory;

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    let products = await Products.findAll({
      include: [{ model: ProductsCategory }],
    });
    products= products.map(product=>product.get({plain:true}))
    const formattedProducts = products.map((product:any) => {
      const category = product.ProductsCategory;
      return {
        id: product.id,
        name: product.name,
        price: product.price,
        description: product.description,
        imageUrl: product.imageUrl,
        categoryName: category.name,
      };
    });
    res.json(formattedProducts);
  } catch (error) {
    next(error);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const newProduct = Products.create(req.body);
    res.status(201).json(newProduct);
  } catch (error) {
    console.log(error);
    next(error);
  }
}); 

router.get('/:id', async (req, res, next) => {
  try {
    const product = await Products.findById(req.params.id);
    if (!product) {
      return res.status(404).json({ error: 'Products not found' });
    }
    res.json(product);
  } catch (error) {
    next(error);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    const updatedProduct = await Products.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!updatedProduct) {
      return res.status(404).json({ error: 'Products not found' });
    }
    res.json(updatedProduct);
  } catch (error) {
    next(error);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    console.log("test",req.params.id);
    const deletedProduct = await Products.findByPk(req.params.id);
    if (!deletedProduct) {
      return res.status(404).json({ error: 'Products not found' });
    }
    deletedProduct.destroy();
    res.sendStatus(204).json("delete ok");
  } catch (error) {
    next(error);
  }
});

module.exports = router;