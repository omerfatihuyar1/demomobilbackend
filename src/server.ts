import * as express from 'express';
const app = express();
const port = 3000;

// API rotalarını tanımlayın
app.get('/', (req, res) => {
  res.send('TEST');
});

// Server'ı dinlemeye başlayın
app.listen(port, () => {
  console.log(`Server http://localhost:${port} adresinde çalışıyor.`);
});