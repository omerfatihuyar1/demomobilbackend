module.exports = (sequelize, DataTypes) => {
    const ProductsCategory = sequelize.define(
      "ProductsCategory",
      {
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          primaryKey: true,
          defaultValue: DataTypes.UUIDV4,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        tableName: "productsCategory",
        timestamps: false,
      }
    );
  
    return ProductsCategory;
  };