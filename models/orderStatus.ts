module.exports = (sequelize, DataTypes) => {
    const OrderStatus = sequelize.define(
      "OrderStatus",
      {
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          primaryKey: true,
          defaultValue: DataTypes.UUIDV4,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING,
          allowNull: true,
        },
      },
      {
        tableName: "orderStatus",
        timestamps: false,
      }
    );
  
    return OrderStatus;
  };