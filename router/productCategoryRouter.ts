import * as express from 'express';
import db from '../models/index';

var ProductsCategory = db.productCategory;

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const categories = await ProductsCategory.findAll();
    res.json(categories);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

router.post('/', async (req, res) => {
  try {
    const { name } = req.body;
    const category = await ProductsCategory.create({ name });
    res.json(category);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { name } = req.body;
    const category = await ProductsCategory.findByPk(id);
    if (category) {
      category.name = name;
      await category.save();
      res.json(category);
    } else {
      res.status(404).json({ error: 'Category not found' });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Belirli bir kategoriyi silme
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const category = await ProductsCategory.findByPk(id);
    if (category) {
      await category.destroy();
      res.json({ message: 'Category deleted' });
    } else {
      res.status(404).json({ error: 'Category not found' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

module.exports = router;