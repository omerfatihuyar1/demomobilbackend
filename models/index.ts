import { Sequelize } from 'sequelize';
const dbConfig = require("../config/database");

const dbSequelize = new Sequelize(
  dbConfig.development.database,
  dbConfig.development.username,
  dbConfig.development.password,
  {
    logging: false,
    host: dbConfig.development.host,
    dialect: dbConfig.development.dialect,
    dialectOptions: { decimalNumbers: true },
    port: dbConfig.development.port,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  }
);

const db: any = {
  Sequelize,
  sequelize: dbSequelize,
};

db.users = require('./users')(dbSequelize, Sequelize);
db.roles = require('./roles')(dbSequelize, Sequelize);
db.orders = require('./orders')(dbSequelize, Sequelize);
db.products = require('./products')(dbSequelize, Sequelize);
db.orderStatus = require('./orderStatus')(dbSequelize, Sequelize);
db.productCategory = require('./productsCategory')(dbSequelize, Sequelize);

db.users.belongsTo(db.roles, { foreignKey: 'roleId', onDelete: 'restrict', onUpdate: 'restrict' });
db.orders.belongsTo(db.users, { foreignKey: 'userId', onDelete: 'restrict', onUpdate: 'restrict' });
db.products.belongsTo(db.productCategory, { foreignKey: 'categoryId', onDelete: 'restrict', onUpdate: 'restrict' });

export default db;
