# DemoMobil

Proje açıklaması buraya yazılır.

# Kullanılan Teknolojiler

Express.js 
Nodemon 
Sequelize

## Kurulum

1. Proje deposunu yerel makinenize klonlayın:

   git clone https://gitlab.com/omerfatihuyar1/demomobilbackend.git
2. Postgresql bağlantı bilgileri ekleyin (./config/database.js).

3. İlgili dizine gidip sırasıyla komutları terminalde çalıştırın.
   npm install
   npm start

4. Synced db yazısı terminalde yazana kadar bekleyin.